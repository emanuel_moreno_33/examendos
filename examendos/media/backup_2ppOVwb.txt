--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: envioproducto_envios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.envioproducto_envios (
    id integer NOT NULL,
    producto character varying(70) NOT NULL,
    cantidad integer NOT NULL,
    paqueteria character varying(50) NOT NULL,
    tipoenvio character varying(50) NOT NULL,
    prioridad character varying(50) NOT NULL,
    direccion text NOT NULL,
    codigopostal integer NOT NULL,
    fechaenvio date NOT NULL,
    fechallegada date NOT NULL,
    slug character varying(50) NOT NULL
);


ALTER TABLE public.envioproducto_envios OWNER TO postgres;

--
-- Name: envioproducto_envios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.envioproducto_envios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.envioproducto_envios_id_seq OWNER TO postgres;

--
-- Name: envioproducto_envios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.envioproducto_envios_id_seq OWNED BY public.envioproducto_envios.id;


--
-- Name: garaje_autos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.garaje_autos (
    id integer NOT NULL,
    modelo character varying(50) NOT NULL,
    marca character varying(50) NOT NULL,
    tipo character varying(50) NOT NULL,
    placa character varying(15) NOT NULL,
    fecha_registro date NOT NULL,
    slug character varying(50) NOT NULL
);


ALTER TABLE public.garaje_autos OWNER TO postgres;

--
-- Name: garaje_autos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.garaje_autos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garaje_autos_id_seq OWNER TO postgres;

--
-- Name: garaje_autos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.garaje_autos_id_seq OWNED BY public.garaje_autos.id;


--
-- Name: informeservicio_informes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.informeservicio_informes (
    id integer NOT NULL,
    nocuenta integer NOT NULL,
    noreporte integer NOT NULL,
    fecharealizacion timestamp with time zone NOT NULL,
    fechatermino timestamp with time zone NOT NULL,
    personarealizo character varying(50) NOT NULL,
    materialesproporcionados character varying(50) NOT NULL,
    motivonorealizacion character varying(50),
    notas text,
    slug character varying(50) NOT NULL
);


ALTER TABLE public.informeservicio_informes OWNER TO postgres;

--
-- Name: informeservicio_informes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.informeservicio_informes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.informeservicio_informes_id_seq OWNER TO postgres;

--
-- Name: informeservicio_informes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.informeservicio_informes_id_seq OWNED BY public.informeservicio_informes.id;


--
-- Name: nflp_equipos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nflp_equipos (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apodo character varying(50) NOT NULL,
    lugar_origen character varying(50) NOT NULL,
    campeonatos integer NOT NULL
);


ALTER TABLE public.nflp_equipos OWNER TO postgres;

--
-- Name: nflp_equipos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nflp_equipos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nflp_equipos_id_seq OWNER TO postgres;

--
-- Name: nflp_equipos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nflp_equipos_id_seq OWNED BY public.nflp_equipos.id;


--
-- Name: nflp_estadios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nflp_estadios (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    lugar_origen character varying(50) NOT NULL,
    equipo character varying(50) NOT NULL
);


ALTER TABLE public.nflp_estadios OWNER TO postgres;

--
-- Name: nflp_estadios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nflp_estadios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nflp_estadios_id_seq OWNER TO postgres;

--
-- Name: nflp_estadios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nflp_estadios_id_seq OWNED BY public.nflp_estadios.id;


--
-- Name: nflp_jugador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nflp_jugador (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50) NOT NULL,
    apodo character varying(50) NOT NULL,
    numero integer NOT NULL,
    equipo character varying(50) NOT NULL,
    posicion character varying(50) NOT NULL,
    status boolean NOT NULL
);


ALTER TABLE public.nflp_jugador OWNER TO postgres;

--
-- Name: nflp_jugador_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nflp_jugador_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nflp_jugador_id_seq OWNER TO postgres;

--
-- Name: nflp_jugador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nflp_jugador_id_seq OWNED BY public.nflp_jugador.id;


--
-- Name: pokedex_pokemon; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pokedex_pokemon (
    id integer NOT NULL,
    nombre character varying(32) NOT NULL,
    tipo character varying(32) NOT NULL,
    nivel integer NOT NULL,
    exp integer NOT NULL,
    status boolean NOT NULL,
    "timestamp" date NOT NULL,
    slug character varying(50) NOT NULL,
    trainer_id integer NOT NULL
);


ALTER TABLE public.pokedex_pokemon OWNER TO postgres;

--
-- Name: pokedex_pokemon_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pokedex_pokemon_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pokedex_pokemon_id_seq OWNER TO postgres;

--
-- Name: pokedex_pokemon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pokedex_pokemon_id_seq OWNED BY public.pokedex_pokemon.id;


--
-- Name: registroalumnos_alumnos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registroalumnos_alumnos (
    id integer NOT NULL,
    nocontrol integer NOT NULL,
    nombre character varying(70) NOT NULL,
    apepat character varying(70) NOT NULL,
    apemat character varying(70) NOT NULL,
    carrera character varying(5) NOT NULL,
    fechanac date NOT NULL,
    tiposangre character varying(5) NOT NULL,
    correo character varying(254) NOT NULL,
    slug character varying(50) NOT NULL
);


ALTER TABLE public.registroalumnos_alumnos OWNER TO postgres;

--
-- Name: registroalumnos_alumnos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.registroalumnos_alumnos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registroalumnos_alumnos_id_seq OWNER TO postgres;

--
-- Name: registroalumnos_alumnos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.registroalumnos_alumnos_id_seq OWNED BY public.registroalumnos_alumnos.id;


--
-- Name: registrocomentario_comentarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registrocomentario_comentarios (
    id integer NOT NULL,
    usuario character varying(50) NOT NULL,
    "contraseña" character varying(50) NOT NULL,
    nosolicitud integer NOT NULL,
    tiposervicio character varying(50) NOT NULL,
    fechapublicacion date NOT NULL,
    comentario text NOT NULL,
    slug character varying(50) NOT NULL
);


ALTER TABLE public.registrocomentario_comentarios OWNER TO postgres;

--
-- Name: registrocomentario_comentarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.registrocomentario_comentarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registrocomentario_comentarios_id_seq OWNER TO postgres;

--
-- Name: registrocomentario_comentarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.registrocomentario_comentarios_id_seq OWNED BY public.registrocomentario_comentarios.id;


--
-- Name: registroelectronico_electronicos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registroelectronico_electronicos (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    categoria character varying(50) NOT NULL,
    total integer NOT NULL,
    disponible integer NOT NULL,
    ubicacion character varying(50) NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.registroelectronico_electronicos OWNER TO postgres;

--
-- Name: registroelectronico_electronicos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.registroelectronico_electronicos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registroelectronico_electronicos_id_seq OWNER TO postgres;

--
-- Name: registroelectronico_electronicos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.registroelectronico_electronicos_id_seq OWNED BY public.registroelectronico_electronicos.id;


--
-- Name: registromedico_registros; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registromedico_registros (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    edad integer NOT NULL,
    presion double precision NOT NULL,
    temperatura double precision NOT NULL,
    sexo character varying(1) NOT NULL,
    sintomas text NOT NULL,
    medico character varying(50) NOT NULL,
    fechaconsulta date NOT NULL,
    notas text NOT NULL,
    slug character varying(50) NOT NULL
);


ALTER TABLE public.registromedico_registros OWNER TO postgres;

--
-- Name: registromedico_registros_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.registromedico_registros_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registromedico_registros_id_seq OWNER TO postgres;

--
-- Name: registromedico_registros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.registromedico_registros_id_seq OWNED BY public.registromedico_registros.id;


--
-- Name: registroropa_mangacorta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registroropa_mangacorta (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    color character varying(50) NOT NULL,
    marca character varying(100) NOT NULL,
    licencia character varying(100) NOT NULL,
    categoria character varying(100) NOT NULL,
    usando boolean NOT NULL,
    slug character varying(50) NOT NULL
);


ALTER TABLE public.registroropa_mangacorta OWNER TO postgres;

--
-- Name: registroropa_mangacorta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.registroropa_mangacorta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registroropa_mangacorta_id_seq OWNER TO postgres;

--
-- Name: registroropa_mangacorta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.registroropa_mangacorta_id_seq OWNED BY public.registroropa_mangacorta.id;


--
-- Name: solicitudempleo_solicitudes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitudempleo_solicitudes (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    appat character varying(50) NOT NULL,
    apmat character varying(50) NOT NULL,
    fechanac date NOT NULL,
    lugarorigen character varying(50) NOT NULL,
    "CURP" character varying(20) NOT NULL,
    gradoestudios character varying(50) NOT NULL,
    aniosexplaboral integer NOT NULL,
    correo character varying(254) NOT NULL,
    dineroganar numeric(8,2) NOT NULL
);


ALTER TABLE public.solicitudempleo_solicitudes OWNER TO postgres;

--
-- Name: solicitudempleo_solicitudes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitudempleo_solicitudes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitudempleo_solicitudes_id_seq OWNER TO postgres;

--
-- Name: solicitudempleo_solicitudes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitudempleo_solicitudes_id_seq OWNED BY public.solicitudempleo_solicitudes.id;


--
-- Name: solicitudservicio_servicios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitudservicio_servicios (
    id integer NOT NULL,
    nocuenta integer NOT NULL,
    tiposervicio character varying(50) NOT NULL,
    "Motivo" character varying(50) NOT NULL,
    "FechaVisita" date NOT NULL,
    "Momentodia" character varying(50) NOT NULL,
    "Solicitante" character varying(50) NOT NULL,
    "Parentesco" character varying(50) NOT NULL,
    "FechaCreacion" date NOT NULL,
    "Notas" text NOT NULL,
    slug character varying(50) NOT NULL
);


ALTER TABLE public.solicitudservicio_servicios OWNER TO postgres;

--
-- Name: solicitudservicio_servicios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitudservicio_servicios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitudservicio_servicios_id_seq OWNER TO postgres;

--
-- Name: solicitudservicio_servicios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitudservicio_servicios_id_seq OWNED BY public.solicitudservicio_servicios.id;


--
-- Name: envioproducto_envios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.envioproducto_envios ALTER COLUMN id SET DEFAULT nextval('public.envioproducto_envios_id_seq'::regclass);


--
-- Name: garaje_autos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.garaje_autos ALTER COLUMN id SET DEFAULT nextval('public.garaje_autos_id_seq'::regclass);


--
-- Name: informeservicio_informes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.informeservicio_informes ALTER COLUMN id SET DEFAULT nextval('public.informeservicio_informes_id_seq'::regclass);


--
-- Name: nflp_equipos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nflp_equipos ALTER COLUMN id SET DEFAULT nextval('public.nflp_equipos_id_seq'::regclass);


--
-- Name: nflp_estadios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nflp_estadios ALTER COLUMN id SET DEFAULT nextval('public.nflp_estadios_id_seq'::regclass);


--
-- Name: nflp_jugador id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nflp_jugador ALTER COLUMN id SET DEFAULT nextval('public.nflp_jugador_id_seq'::regclass);


--
-- Name: pokedex_pokemon id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pokedex_pokemon ALTER COLUMN id SET DEFAULT nextval('public.pokedex_pokemon_id_seq'::regclass);


--
-- Name: registroalumnos_alumnos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registroalumnos_alumnos ALTER COLUMN id SET DEFAULT nextval('public.registroalumnos_alumnos_id_seq'::regclass);


--
-- Name: registrocomentario_comentarios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registrocomentario_comentarios ALTER COLUMN id SET DEFAULT nextval('public.registrocomentario_comentarios_id_seq'::regclass);


--
-- Name: registroelectronico_electronicos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registroelectronico_electronicos ALTER COLUMN id SET DEFAULT nextval('public.registroelectronico_electronicos_id_seq'::regclass);


--
-- Name: registromedico_registros id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registromedico_registros ALTER COLUMN id SET DEFAULT nextval('public.registromedico_registros_id_seq'::regclass);


--
-- Name: registroropa_mangacorta id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registroropa_mangacorta ALTER COLUMN id SET DEFAULT nextval('public.registroropa_mangacorta_id_seq'::regclass);


--
-- Name: solicitudempleo_solicitudes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitudempleo_solicitudes ALTER COLUMN id SET DEFAULT nextval('public.solicitudempleo_solicitudes_id_seq'::regclass);


--
-- Name: solicitudservicio_servicios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitudservicio_servicios ALTER COLUMN id SET DEFAULT nextval('public.solicitudservicio_servicios_id_seq'::regclass);


--
-- Data for Name: envioproducto_envios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.envioproducto_envios (id, producto, cantidad, paqueteria, tipoenvio, prioridad, direccion, codigopostal, fechaenvio, fechallegada, slug) FROM stdin;
2	Clavos	5	Fedex	Nacional	Urgente	Av. de las tortolas 435-A	12345	2019-09-05	2019-09-06	Envio2
3	Ventiladores	1	Estafeta	Nacional	Dia siguiente	Blvd. industrial 1234-12	12346	2019-09-05	2019-09-06	Envio3
4	Computadora	4	UPS	Internacional	Dia siguiente	Blvd. Insurgents 1278-78	20900	2019-09-05	2019-09-06	Envio4
5	Silla	3	Castores	Nacional	Urgente	Calle 2 Norte 346 S/N	16849	2019-09-05	2019-09-09	Envio5
6	Triplay de madera	3	UPS	Nacional	Normal	Av de los insurgentes.	14630	2019-09-09	2019-09-09	Envio6
1	Tornillo	4	UPS	Nacional	Normal	Av. de las flores 12354 S/N	22768	2019-09-05	2019-09-07	Envio1
\.


--
-- Data for Name: garaje_autos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.garaje_autos (id, modelo, marca, tipo, placa, fecha_registro, slug) FROM stdin;
2	XA-21	Ocelot	Super	speed	2019-09-04	XA-21
3	Zentorno	Pegassi	Super	lambo	2019-09-04	Zentorno
4	9F Cabrio	Obey	Deportivo	2046	2019-09-04	9FCabrio
5	Rosevelt	Albany	Deportivo clasico	clasico	2019-09-04	Rosevelt
6	Sabre Turbo	Declasse	Bolido	clasico2	2019-09-04	SabreTurbo
7	Bati 801	Pegassi	Motocicletas	Moto02	2019-09-04	Bati801
8	S80RR	Annis	Super	speed	2019-09-09	S80RR
9	Jester Clasico	Dinka	Deportivo	deportivo1	2019-09-09	JesterClasico
1	Akima	Dinka	Motocicletas	Moto01	2019-09-04	Akuma
\.


--
-- Data for Name: informeservicio_informes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.informeservicio_informes (id, nocuenta, noreporte, fecharealizacion, fechatermino, personarealizo, materialesproporcionados, motivonorealizacion, notas, slug) FROM stdin;
2	1236	2	2019-09-08 19:22:07-07	2019-09-09 19:22:09-07	Luis	ninguno	\N	Se realizo con problema en instalacion de tuberia.	Servicio2
3	12347	3	2019-09-09 19:30:05-07	2019-09-11 19:30:08-07	Antonio	Tuberia	No estaba presente	Se realizo segunda visita.	Servicio3
4	12349	4	2019-09-08 19:34:37-07	2019-09-08 19:34:38-07	Antonio	ninguno	\N	Ninguna	Servicio4
5	41308	5	2019-09-09 19:35:05-07	2019-09-09 19:35:08-07	Antonio	ninguno	No estaba presente	Ninguna	Servicio5
6	1587	15	2019-09-08 19:35:00-07	2019-09-08 20:35:00-07	Antonio	Tuberia	No estaba presente	Ninguna	Servicio7
1	1234	1	2019-09-08 19:21:19-07	2019-09-08 19:21:22-07	Luis	ninguno	\N	Se realizo sin problemas	Servicio1
\.


--
-- Data for Name: nflp_equipos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nflp_equipos (id, nombre, apodo, lugar_origen, campeonatos) FROM stdin;
1	Patriotas de nueva inglaterra	Patriotas	Massachusetts	6
2	Empacadores de green bay	Empacadores	Green Bay	4
3	Osos de chicago	Osos	Chicago	1
\.


--
-- Data for Name: nflp_estadios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nflp_estadios (id, nombre, lugar_origen, equipo) FROM stdin;
3	Lambeau Field	Green Bay	Empacadores de green bay
4	Gilette Stadium	Massachusetts	Patriotas de nueva inglaterra
5	Soldier Field	Chicago	Osos de chicago
\.


--
-- Data for Name: nflp_jugador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nflp_jugador (id, nombre, apellido, apodo, numero, equipo, posicion, status) FROM stdin;
1	Tom	Brady	Touchdown Tom	12	Patriotas de nueva inglaterra	Quarterback	t
2	Julian	Francis	Julian	11	Patriotas de nueva inglaterra	Receptor	t
3	Jamie	Collins	Jamie	58	Patriotas de nueva inglaterra	Linebacker	t
4	Aaron	Rodgers	Aaron	12	Empacadores de green bay	Quarterback	t
5	Davante	Adams	Davante	15	Empacadores de green bay	Receptor	t
6	David	Bakhtiari	David	69	Empacadores de green bay	Tacleador	t
7	Khalil	Mack	Khalil	52	Osos de chicago	Linebacker	t
8	Claude	Jackson	Speed	21	Osos de chicago	Esquinero	t
9	Darryl Frank	Skrine	Buster	24	Osos de chicago	Esquinero	t
10	Emanuel	Moreno	Claudio	33	Patriotas de nueva inglaterra	Quarterback	t
\.


--
-- Data for Name: pokedex_pokemon; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pokedex_pokemon (id, nombre, tipo, nivel, exp, status, "timestamp", slug, trainer_id) FROM stdin;
4	Bulbasaur	Planta	2	1200	t	2019-09-08	Bulbasaur	1
5	Metapod	Bicho	2	1300	t	2019-09-11	Metapod	1
2	squirtle	agua	2	190	t	2019-09-03	squarttle	1
11	Abra	Psyco	1	1500	t	2019-09-12	Abra	1
1	pikachu	electrico	2	2500	f	2019-09-03	pikachu	1
\.


--
-- Data for Name: registroalumnos_alumnos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registroalumnos_alumnos (id, nocontrol, nombre, apepat, apemat, carrera, fechanac, tiposangre, correo, slug) FROM stdin;
2	12345678	sad	sdfa	asf	ISIC	1997-12-21	O+	emadsd@sdf.com	12345678
3	12345796	Luis	Lopez	asd	IIND	2019-09-04	A-	AFSD@SDFA.COM	12345796
4	12789564	Sofia	Hernandez	Velazquez	IINF	2019-09-04	O+	AFSD@SDFA.COM	12789564
5	19640578	Claudio	Moreno	Romero	ISIC	2019-09-04	A-	AFSD@SDFA.COM	19640578
6	16450890	Emanuel	Hernandez	Ramos	IINF	1997-12-21	O+	emadsd@sdf.com	Emanuel
1	16212046	Emanuel	Moreno	Ramos	ISIC	1997-12-21	O+	emadsd@sdf.com	16212046
\.


--
-- Data for Name: registrocomentario_comentarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registrocomentario_comentarios (id, usuario, "contraseña", nosolicitud, tiposervicio, fechapublicacion, comentario, slug) FROM stdin;
2	emanuel_moreno	asdf	2	Reparacion	2019-09-09	Lo dejaron mal construido	Servicio2
3	flor33	asdf	3	Conexion	2019-09-09	No me gusto el servicio	Servicio3
4	ironman	asdf	4	Reconexion	2019-09-09	gg	Servicio4
5	daft_punk_21	asdf	5	Reparacion	2019-09-09	Se daño mas de lo que se arreglo.	Servicio5
8	taleof_us	asdf	12	Reparacion	2019-09-09	No arreglaron lo que quitaron	Servicio8
1	claudespeed2133	asdf	1	Reconexion	2019-09-09	Todo bien	Servicio1
\.


--
-- Data for Name: registroelectronico_electronicos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registroelectronico_electronicos (id, nombre, categoria, total, disponible, ubicacion, descripcion) FROM stdin;
2	7432	compuertas	2	2	Caja 3	compuerta or
3	L2993D	circuito integrado	3	3	caja 2	puente h para controlar motores
4	7408	compuertas	5	5	caja 2	compuerta and
5	DA05	display	7	7	caja 2	display de 10 pines muestra numero y punto decimal.
6	push button 4	interruptores	1	1	caja 3	push button de 4 patas
7	fotoresistencia	resistencia	5	5	caja 3	fotoresistencia.
8	7805	reguladores	0	0	caja 3	reguladores de voltaje positivo
10	DHT11	Arduino	1	1	Caja arduino	Sensor de temperatura y humedad, tiene 4 pines pero 3 solo se usan.
11	7447	Decodificadores	7	7	caja 3	Decodificador para display
9	Arduino Uno	Arduino	4	3	Caja arduino	Microcontrolador basado en ATmega328P con 14 pines digitales y 5 analogicos
\.


--
-- Data for Name: registromedico_registros; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registromedico_registros (id, nombre, edad, presion, temperatura, sexo, sintomas, medico, fechaconsulta, notas, slug) FROM stdin;
2	Luis	21	12.3000000000000007	34	M	Tos y cansancio	Juan	2019-09-04	solo se llevo un jarabe	Luis
3	Maria	20	95	29	F	dolor de cabeza	Luis	2019-09-04	se llevo un paracetamol	Maria
4	Emanuel	21	90	36.1000000000000014	M	dolor de estomago	Juan	2019-09-04	se llevo un omeprazol	Emanuel
5	Sofia	19	89	25.8000000000000007	F	se tallaba mucho el ojo	Luis	2019-09-04	se le dieron unas gotas para los ojos	Sofia
6	Emanuel	21	178	23	M	Dolor de cabeza	Luis	2019-09-09	Se llevo una aspirina	Emanuel
1	Emanuel	21	13	13.4000000000000004	M	Golpe de calor	Juan	2019-09-04	tomo un suero	Emanuel
\.


--
-- Data for Name: registroropa_mangacorta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registroropa_mangacorta (id, nombre, color, marca, licencia, categoria, usando, slug) FROM stdin;
2	Crush	negro	mascara de latex	daft punk	musica	t	Crush
3	Get Lucky	negro	mascara de latex	daft punk	musica	t	GetLucky
4	Uruapan Mexican Lucky	gris	mascara de latex	daft punk	musica	t	UruapanMexicanLucky
5	Traje Mascara de Latex	negro	mascara de latex	otros	otros	t	TrajeMascaradeLatex
6	Star Lord	gris claro	mascara de latex	marvel	marvel	t	StarLord
7	Boba Fett	gris con blanco	mascara de latex	star wars	star wars	t	BobaFett
8	Groaaar!	azul con blanco	mascara de latex	star wars	star wars	t	Groaaar
10	Traje Leonardo	verde	mascara de latex	tortugas ninja	peliculas	t	TrajeLeonardo
11	Megatron	negro	mascara de latex	transformers	peliculas	t	Megatron
9	Vader	gris	mascara de latex	star wars	star wars	t	Vader
1	RAM	negro	mascara de latex	daft punk	musica	t	RAM
\.


--
-- Data for Name: solicitudempleo_solicitudes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitudempleo_solicitudes (id, nombre, appat, apmat, fechanac, lugarorigen, "CURP", gradoestudios, aniosexplaboral, correo, dineroganar) FROM stdin;
2	Luis	Lopez	Flores	1997-09-09	TIjuana	ASDF1750HRRMOO00	Preparatoria completa	3	emadsd@sdf.com	150.50
3	Sofia	Ramirez	Garcia	1997-01-01	Mexicali	ASDF1750HRRMOO00	Universidad incompleta	2	AFSD@SDFA.COM	1500.76
4	Claudio	Lopez	Flores	1997-12-21	TIjuana	ASDF1750HRRMOO00	Preparatoria completa	6	AFSD@SDFA.COM	330.33
5	Maria	Moreno	Garcia	2019-09-09	Mexicali	ASDF1750HRRMOO00	Preparatoria completa	3	AFSD@SDFA.COM	1200.00
6	Emanuel	Moreno	Garcia	1997-12-21	TIjuana	ASDF1750HRRMOO00	Universidad completa	1	AFSD@SDFA.COM	200.70
1	Emanuel	Moreno	Ramos	1997-12-21	TIjuana	ASDF1750HRRMOO00	Preparatoria completa	1	emadsd@sdf.com	100.00
\.


--
-- Data for Name: solicitudservicio_servicios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitudservicio_servicios (id, nocuenta, tiposervicio, "Motivo", "FechaVisita", "Momentodia", "Solicitante", "Parentesco", "FechaCreacion", "Notas", slug) FROM stdin;
5	1085	Reparacion	Robo	2019-09-10	Primera hora	Fernando	Hijo	2019-09-08	Llevar tuberia extra.	Servicio3
6	4910	Reparacion	Dererioro	2019-09-08	Medio dia	Sofia	Hija	2019-09-08	Ellos tienen el material.	Servicio4
7	4687	Reconexion	Completo pago	2019-09-08	Despues de medio dia	Fernando	Dueño	2019-09-09	ninguna	Servicio5
8	546465	Reconexion	Robo	2019-09-08	Primera hora	Maria	Dueño	2019-09-09	no hay tuberias	Servicio1
9	546445	Reconexion	Robo	2019-09-08	Primera hora	Luisa	Dueño	2019-09-09	no hay tuberias	Servicio2
3	1234	Reconexion	Completo pago	2019-09-09	Primera hora	Emanuel	Hijo	2019-09-08	sad	Servicio1
4	1478	Conexion	Nueva cuenta	2019-09-08	Medio dia	Luis	Dueño	2019-09-08	sda	Servicio2
\.


--
-- Name: envioproducto_envios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.envioproducto_envios_id_seq', 7, true);


--
-- Name: garaje_autos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.garaje_autos_id_seq', 9, true);


--
-- Name: informeservicio_informes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.informeservicio_informes_id_seq', 6, true);


--
-- Name: nflp_equipos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nflp_equipos_id_seq', 3, true);


--
-- Name: nflp_estadios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nflp_estadios_id_seq', 5, true);


--
-- Name: nflp_jugador_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nflp_jugador_id_seq', 42, true);


--
-- Name: pokedex_pokemon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pokedex_pokemon_id_seq', 11, true);


--
-- Name: registroalumnos_alumnos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.registroalumnos_alumnos_id_seq', 6, true);


--
-- Name: registrocomentario_comentarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.registrocomentario_comentarios_id_seq', 8, true);


--
-- Name: registroelectronico_electronicos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.registroelectronico_electronicos_id_seq', 11, true);


--
-- Name: registromedico_registros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.registromedico_registros_id_seq', 7, true);


--
-- Name: registroropa_mangacorta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.registroropa_mangacorta_id_seq', 11, true);


--
-- Name: solicitudempleo_solicitudes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitudempleo_solicitudes_id_seq', 6, true);


--
-- Name: solicitudservicio_servicios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitudservicio_servicios_id_seq', 9, true);


--
-- Name: envioproducto_envios envioproducto_envios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.envioproducto_envios
    ADD CONSTRAINT envioproducto_envios_pkey PRIMARY KEY (id);


--
-- Name: garaje_autos garaje_autos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.garaje_autos
    ADD CONSTRAINT garaje_autos_pkey PRIMARY KEY (id);


--
-- Name: informeservicio_informes informeservicio_informes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.informeservicio_informes
    ADD CONSTRAINT informeservicio_informes_pkey PRIMARY KEY (id);


--
-- Name: nflp_equipos nflp_equipos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nflp_equipos
    ADD CONSTRAINT nflp_equipos_pkey PRIMARY KEY (id);


--
-- Name: nflp_estadios nflp_estadios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nflp_estadios
    ADD CONSTRAINT nflp_estadios_pkey PRIMARY KEY (id);


--
-- Name: nflp_jugador nflp_jugador_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nflp_jugador
    ADD CONSTRAINT nflp_jugador_pkey PRIMARY KEY (id);


--
-- Name: pokedex_pokemon pokedex_pokemon_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pokedex_pokemon
    ADD CONSTRAINT pokedex_pokemon_pkey PRIMARY KEY (id);


--
-- Name: registroalumnos_alumnos registroalumnos_alumnos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registroalumnos_alumnos
    ADD CONSTRAINT registroalumnos_alumnos_pkey PRIMARY KEY (id);


--
-- Name: registrocomentario_comentarios registrocomentario_comentarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registrocomentario_comentarios
    ADD CONSTRAINT registrocomentario_comentarios_pkey PRIMARY KEY (id);


--
-- Name: registroelectronico_electronicos registroelectronico_electronicos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registroelectronico_electronicos
    ADD CONSTRAINT registroelectronico_electronicos_pkey PRIMARY KEY (id);


--
-- Name: registromedico_registros registromedico_registros_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registromedico_registros
    ADD CONSTRAINT registromedico_registros_pkey PRIMARY KEY (id);


--
-- Name: registroropa_mangacorta registroropa_mangacorta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.registroropa_mangacorta
    ADD CONSTRAINT registroropa_mangacorta_pkey PRIMARY KEY (id);


--
-- Name: solicitudempleo_solicitudes solicitudempleo_solicitudes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitudempleo_solicitudes
    ADD CONSTRAINT solicitudempleo_solicitudes_pkey PRIMARY KEY (id);


--
-- Name: solicitudservicio_servicios solicitudservicio_servicios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitudservicio_servicios
    ADD CONSTRAINT solicitudservicio_servicios_pkey PRIMARY KEY (id);


--
-- Name: envioproducto_envios_slug_9f1ef0ef; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX envioproducto_envios_slug_9f1ef0ef ON public.envioproducto_envios USING btree (slug);


--
-- Name: envioproducto_envios_slug_9f1ef0ef_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX envioproducto_envios_slug_9f1ef0ef_like ON public.envioproducto_envios USING btree (slug varchar_pattern_ops);


--
-- Name: garaje_autos_slug_d632338c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX garaje_autos_slug_d632338c ON public.garaje_autos USING btree (slug);


--
-- Name: garaje_autos_slug_d632338c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX garaje_autos_slug_d632338c_like ON public.garaje_autos USING btree (slug varchar_pattern_ops);


--
-- Name: informeservicio_informes_slug_53e1c05b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX informeservicio_informes_slug_53e1c05b ON public.informeservicio_informes USING btree (slug);


--
-- Name: informeservicio_informes_slug_53e1c05b_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX informeservicio_informes_slug_53e1c05b_like ON public.informeservicio_informes USING btree (slug varchar_pattern_ops);


--
-- Name: pokedex_pokemon_slug_c0d24083; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pokedex_pokemon_slug_c0d24083 ON public.pokedex_pokemon USING btree (slug);


--
-- Name: pokedex_pokemon_slug_c0d24083_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pokedex_pokemon_slug_c0d24083_like ON public.pokedex_pokemon USING btree (slug varchar_pattern_ops);


--
-- Name: pokedex_pokemon_trainer_id_fc375656; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pokedex_pokemon_trainer_id_fc375656 ON public.pokedex_pokemon USING btree (trainer_id);


--
-- Name: registroalumnos_alumnos_slug_8447d691; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX registroalumnos_alumnos_slug_8447d691 ON public.registroalumnos_alumnos USING btree (slug);


--
-- Name: registroalumnos_alumnos_slug_8447d691_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX registroalumnos_alumnos_slug_8447d691_like ON public.registroalumnos_alumnos USING btree (slug varchar_pattern_ops);


--
-- Name: registrocomentario_comentarios_slug_b1b5d8a0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX registrocomentario_comentarios_slug_b1b5d8a0 ON public.registrocomentario_comentarios USING btree (slug);


--
-- Name: registrocomentario_comentarios_slug_b1b5d8a0_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX registrocomentario_comentarios_slug_b1b5d8a0_like ON public.registrocomentario_comentarios USING btree (slug varchar_pattern_ops);


--
-- Name: registromedico_registros_slug_263019b3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX registromedico_registros_slug_263019b3 ON public.registromedico_registros USING btree (slug);


--
-- Name: registromedico_registros_slug_263019b3_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX registromedico_registros_slug_263019b3_like ON public.registromedico_registros USING btree (slug varchar_pattern_ops);


--
-- Name: registroropa_mangacorta_slug_8e86b97e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX registroropa_mangacorta_slug_8e86b97e ON public.registroropa_mangacorta USING btree (slug);


--
-- Name: registroropa_mangacorta_slug_8e86b97e_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX registroropa_mangacorta_slug_8e86b97e_like ON public.registroropa_mangacorta USING btree (slug varchar_pattern_ops);


--
-- Name: solicitudservicio_servicios_slug_11ba5109; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX solicitudservicio_servicios_slug_11ba5109 ON public.solicitudservicio_servicios USING btree (slug);


--
-- Name: solicitudservicio_servicios_slug_11ba5109_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX solicitudservicio_servicios_slug_11ba5109_like ON public.solicitudservicio_servicios USING btree (slug varchar_pattern_ops);


--
-- Name: pokedex_pokemon pokedex_pokemon_trainer_id_fc375656_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pokedex_pokemon
    ADD CONSTRAINT pokedex_pokemon_trainer_id_fc375656_fk_auth_user_id FOREIGN KEY (trainer_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

