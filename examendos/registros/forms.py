from django import forms
from .models import registro


class registroform(forms.ModelForm):
	class Meta:
		model = registro
		fields=[
			"nombreinvestigacion","clase","archivo"
		]
