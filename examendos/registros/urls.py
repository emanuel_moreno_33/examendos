from django.urls import path
from registros import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns=[
	path('create/',views.Crear.as_view(),name="crear"),
	path('general/',views.List.as_view(),name="listageneral"),
	path('categoria1/',views.Listcc.as_view(),name="cc"),
	path('categoria2/',views.Listct.as_view(),name="ct"),
	path('categoria3/',views.Listcn.as_view(),name="cn"),
	path('categoria4/',views.Listcs.as_view(),name="cs"),
	path('categoria5/',views.Listcm.as_view(),name="cm"),
	path('update/<int:pk>/',views.Update.as_view(),name="update1"),
	path('delete/<int:pk>/',views.Delete.as_view(),name="delete1"),
	path('detail/<int:pk>/',views.Detail.as_view(),name="detail1"),

	
]
if settings.DEBUG:

	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns+=  static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)