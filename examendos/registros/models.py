from django.db import models
from django.conf import settings
# Create your models here.

class categoria(models.Model):
	nombrecategoria = models.CharField(max_length=50)

	def __str__(self):
		return self.nombrecategoria

class registro(models.Model):
	nombreinvestigacion = models.CharField(max_length=50)
	usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)
	clase = models.ForeignKey(categoria,on_delete=models.CASCADE)
	archivo = models.FileField(null=True,)
	Fecha_subida = models.DateField(auto_now_add=True)

	def __str__(self):
		return self.nombreinvestigacion
