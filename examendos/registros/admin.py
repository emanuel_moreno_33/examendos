from django.contrib import admin

# Register your models here.
from .models import categoria,registro
#admin.site.register(pokemon)

class admincategoria(admin.ModelAdmin):
	list_display=('id','nombrecategoria')
	list_filter=('id','nombrecategoria')


admin.site.register(categoria,admincategoria)


class registrocategoria(admin.ModelAdmin):
	list_display=('usuario','clase','archivo')
	list_filter=('usuario','clase','archivo')


admin.site.register(registro,registrocategoria)