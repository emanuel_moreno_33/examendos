from django.shortcuts import render,redirect
from django.views import generic
from .models import registro
from .forms import registroform
from django.urls import reverse_lazy
from django.db.models import Q

# Create your views here.

class Crear(generic.CreateView):
	model = registro
	form_class=registroform
	template_name="registros/create.html"
	success_url = '/parte1/general/'


class List(generic.ListView):
	template_name = "registros/list.html"
	queryset = registro.objects.all()


	def get_queryset(self,*args,**kwargs):
		qs= registro.objects.all()
		print(self.request.GET)
		query = self.request.GET.get("q",None)
		if query is not None:
			qs = qs.filter(Q(trainer__username__icontains=query)|Q(nombre__icontains=query))
		return qs

	def get_context_data(self, *args,**kwargs):
		context=super(List,self).get_context_data(*args,**kwargs)
		context ["message"]= "Otro registro"
		return context

class Listcc(generic.ListView):
	template_name = "registros/list.html"
	queryset = registro.objects.filter(clase="1")

class Listct(generic.ListView):
	template_name = "registros/list.html"
	queryset = registro.objects.filter(clase="2")

class Listcn(generic.ListView):
	template_name = "registros/list.html"
	queryset = registro.objects.filter(clase="3")

class Listcs(generic.ListView):
	template_name = "registros/list.html"
	queryset = registro.objects.filter(clase="4")

class Listcm(generic.ListView):
	template_name = "registros/list.html"
	queryset = registro.objects.filter(clase="5")

class Detail(generic.DetailView):
	template_name="registros/detail.html"
	model = registro

class Update(generic.UpdateView):
	model = registro
	form_class = registroform
	template_name="registros/update.html"
	success_url='/parte1/general/'

class Delete(generic.DeleteView):
	model = registro
	form_class = registroform
	template_name="registros/delete.html"
	success_url='/parte1/general/'