from django.urls import path
from home import views
from django.contrib.auth.views import logout_then_login


urlpatterns = [
	path('',views.index,name="login"),
	path('cerrar/',logout_then_login, name="logout"),
	path('signup/',views.signup.as_view(),name="signup"),
	]