from django import forms
from .models import funcion,departamento, software


class funcionform(forms.ModelForm):
	class Meta:
		model = funcion
		fields=[
			"nombre_funcion","descripcion"
		]

class departamentoform(forms.ModelForm):
	class Meta:
		model = departamento
		fields = [
		"nombre_deparatamento","ubicacion"
		]

class softwareform(forms.ModelForm):
	class Meta:
		model = software
		fields = [
		"nombre","desarrollador","funcion","departamento","usuario","slug"
		]
