from django.shortcuts import render,redirect
from django.views import generic
from .models import funcion,departamento, software
from .forms import funcionform,departamentoform,softwareform
from django.urls import reverse_lazy
from django.db.models import Q

# Create your views here.

#crear

class Crearsoftware(generic.CreateView):
	model = software
	form_class=softwareform
	template_name="parrainc/createsoftware.html"
	success_url = '/parte2/listasoft/'

class Creardepartamento(generic.CreateView):
	model = departamento
	form_class=departamentoform
	template_name="parrainc/createdepartamento.html"
	success_url = '/parte2/listadept/'



class Crearfuncion(generic.CreateView):
	model = funcion
	form_class=funcionform
	template_name="parrainc/createfuncion.html"
	success_url = '/parte2/listafunc/'


#listado

class Listsoft2(generic.ListView):
	template_name = "parrainc/filtrado.html"
	queryset = departamento.objects.all()

class Listfilt2(generic.ListView):
	template_name = "parrainc/filtrado2.html"
	queryset = funcion.objects.all()


class Listsoft(generic.ListView):
	template_name = "parrainc/listsoft.html"
	queryset = software.objects.all()

	def get_queryset(self,*args,**kwargs):
		qs= software.objects.all()
		print(self.request.GET)
		query = self.request.GET.get("q",None)
		if query is not None:
			qs = qs.filter(Q(departamento__nombre_deparatamento__icontains=query)|Q(funcion__nombre_funcion__icontains=query))
		return qs

class Listfunc(generic.ListView):
	template_name = "parrainc/listfunc.html"
	queryset = funcion.objects.all()

class Listdept(generic.ListView):
	template_name = "parrainc/listdept.html"
	queryset = departamento.objects.all()

	def get_queryset(self,*args,**kwargs):
		qs= departamento.objects.all()
		print(self.request.GET)
		query = self.request.GET.get("q",None)
		if query is not None:
			qs = qs.filter(Q(nombre_deparatamento__icontains=query))
		return qs


#detalle
class Detailsoft(generic.DetailView):
	template_name="parrainc/detailsoft.html"
	model = software

class Detailfunc(generic.DetailView):
	template_name="parrainc/detailfunc.html"
	model = funcion

class Detaildept(generic.DetailView):
	template_name="parrainc/detaildept.html"
	model = departamento

#actualizar

class Updatesoft(generic.UpdateView):
	model = software
	form_class = softwareform
	template_name="parrainc/updatesoft.html"
	success_url='/parte2/listasoft/'

class Updatefunc(generic.UpdateView):
	model = funcion
	form_class = funcionform
	template_name="parrainc/updatefunc.html"
	success_url='/parte2/listafunc/'

class Updatedept(generic.UpdateView):
	model = departamento
	form_class = departamentoform
	template_name="parrainc/updatedept.html"
	success_url='/parte2/listadept/'

#eliminar
class Deletesoft(generic.DeleteView):
	model = software
	form_class = softwareform
	template_name="parrainc/deletesoft.html"
	success_url='/parte2/listasoft/'

class Deletefunc(generic.DeleteView):
	model = funcion
	form_class = funcionform
	template_name="parrainc/deletefunc.html"
	success_url='/parte2/listafunc/'

class Deletedept(generic.DeleteView):
	model = funcion
	form_class = funcionform
	template_name="parrainc/deletedept.html"
	success_url='/parte2/listadept/'
