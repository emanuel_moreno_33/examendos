from django.db import models
from django.conf import settings


# Create your models here.

class funcion(models.Model):
	nombre_funcion = models.CharField(max_length=50)
	descripcion = models.TextField()

	def __str__(self):
		return self.nombre_funcion

class departamento(models.Model):
	nombre_deparatamento = models.CharField(max_length=50)
	ubicacion = models.CharField(max_length=50)
	fecha_creacion = models.DateField(auto_now_add=True)

	def __str__(self):
		return self.nombre_deparatamento

class software(models.Model):
	nombre = models.CharField(max_length=100)
	desarrollador = models.CharField(max_length=50)
	funcion = models.ForeignKey(funcion,on_delete=models.CASCADE)
	departamento = models.ManyToManyField(departamento)
	Fecha_subida = models.DateField(auto_now_add=True)
	usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	slug = models.SlugField()

	def __str__(self):
		return self.nombre