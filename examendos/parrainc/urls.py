from django.urls import path
from parrainc import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns=[
	path('crearsoftware/',views.Crearsoftware.as_view(),name="crearsoft"),
	path('creardepartamento/',views.Creardepartamento.as_view(),name="creardept"),
	path('crearfuncion/',views.Crearfuncion.as_view(),name="crearfunc"),

	path('listasoft/',views.Listsoft.as_view(),name="listasoft"),
	path('listafunc/',views.Listfunc.as_view(),name="listafunc"),
	path('listadept/',views.Listdept.as_view(),name="listadept"),

	path('updatesoft/<int:pk>/',views.Updatesoft.as_view(),name="updates"),
	path('deletesoft/<int:pk>/',views.Deletesoft.as_view(),name="deletes"),
	path('detailsoft/<int:pk>/',views.Detailsoft.as_view(),name="details"),

	path('updatefunc/<int:pk>/',views.Updatefunc.as_view(),name="updatef"),
	path('deletefunc/<int:pk>/',views.Deletefunc.as_view(),name="deletef"),
	path('detailfunc/<int:pk>/',views.Detailfunc.as_view(),name="detailf"),

	path('updatedept/<int:pk>/',views.Updatedept.as_view(),name="updated"),
	path('deletedept/<int:pk>/',views.Deletedept.as_view(),name="deleted"),
	path('detaildept/<int:pk>/',views.Detaildept.as_view(),name="detaild"),

	path('filtradodept/',views.Listsoft2.as_view(),name="filtradodept"),
	path('filtradofunc/',views.Listfilt2.as_view(),name="filtradofunc"),
]